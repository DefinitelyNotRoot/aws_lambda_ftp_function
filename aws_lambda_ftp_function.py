import ftplib
import os

import boto3

# FTP Credentials
bucketname = os.environ['BUCKETNAME']
ip = os.environ['REMOTE_SERVER']
remote_directory = os.environ['REMOTE_DIRECTORY']
username = os.environ['USERNAME']
password = os.environ['PASSWORD']
NoFileAlertThreshold = int(os.environ['NoFileAlertThreshold'])
KeyFileName = os.environ['KeyFileName']
KeyBucket = os.environ['KeyBucket']
SetDebugLevel = int(os.environ['SetDebugLevel'])

# AWS Clients and Resources
s3 = boto3.resource('s3')
bucket = s3.Bucket(bucketname)
s3client = boto3.client('s3')
cloudwatch = boto3.client('cloudwatch')


def createAlarm(NoFileAlertThreshold):
    cloudwatch.put_metric_alarm(
        AlarmName='No files',
        Period=NoFileAlertThreshold,
        AlarmDescription="No files were downloaded in the last {} minutes".format(NoFileAlertThreshold / 60),
        ActionsEnabled=False,
        MetricName='NumberOfDownloadedFiles',
        Namespace='CDRFTP_Lambda',
        Statistic='Sum',
        Unit='None',
        EvaluationPeriods=1,
        DatapointsToAlarm=1,
        Threshold=1,
        ComparisonOperator='LessThanThreshold',
    )

def pushMetricToCloudwatch(MetricName, unit, metric):
    cloudwatch.put_metric_data(
        MetricData=[
            {
                'MetricName': MetricName,
                'Unit': unit,
                'Value': metric
            },
        ],
        Namespace='CDRFTP_Lambda'
    )

def is_ftp_folder(ftp, filename):
    """
    This function will check if a given name/path is a folder to avoid downloading it
    """
    try:
        res = ftp.sendcmd('MLST ' + filename)
        if 'type=dir;' in res:
            return True
        else:
            return False
    except:
        return False


def get_matching_s3_keys(bucket, prefix='', suffix=''):
    """
    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    kwargs = {'Bucket': bucket, 'Prefix': prefix}
    while True:

        resp = s3client.list_objects_v2(**kwargs)
        try:
            contents = resp['Contents']
        except KeyError:
            return
        for obj in resp['Contents']:
            key = obj['Key']
            if key.endswith(suffix):
                yield key
        try:
            kwargs['ContinuationToken'] = resp['NextContinuationToken']
        except KeyError:
            break


def lambda_handler(event, context):
    # Metrics
    NumberOfDownloadedFiles = 0
    FtpDownloadedSize = 0

    # Setting up the CloudWatch Alarm
    createAlarm(NoFileAlertThreshold)

    # Setting up the key for FTP connection
    if KeyFileName:
        try:
            if os.path.isfile("/tmp/" + KeyFileName):
                print("File {} exists locally, skip".format(KeyFileName))
            else:
                s3.meta.client.download_file(KeyBucket, KeyFileName, '/tmp/' + KeyFileName)
        except:
            print("Error while downloading key from S3 bucket")
            raise


    # Connecting to FTP
    try:
        if KeyFileName:
            if SetDebugLevel:
                ftp = ftplib.FTP(ip)
                ftp.set_debuglevel(SetDebugLevel)
                ftp.login(username, password)
            else:
                ftp = ftplib.FTP(ip)
                ftp.login(username, password)
        else:
            ftp = ftplib.FTP_TLS(ip, keyfile='/tmp/' + KeyFileName)
            ftp.set_debuglevel(SetDebugLevel)
            ftp.login(username, password)
    except:
        print("Error connecting to FTP")
        raise

    # Changing the directory
    try:
        ftp.cwd(remote_directory)
    except:
        print("Error changing to directory {}".format(remote_directory))
        raise

    # Getting the list of files on FTP
    try:
        files = ftp.nlst()
    except:
        print("Error listing the directory")
        raise

    # Getting the list of files on S3 Bucket
    try:
        s3files = list(get_matching_s3_keys(bucket=bucketname))
    except:
        print("Error listing the S3 bucket")
        raise

    for file in files:
        # If file is not directory
        if not is_ftp_folder(ftp, file):
            # If file is not in S3 Bucket
            if file not in s3files:
                print("File {} not exist in S3 bucket: {}".format(file, bucketname))
                try:
                    if os.path.isfile("/tmp/" + file):
                        print("File {} exists locally, skip".format(file))
                    else:
                        if file == "512KB.zip" or file == "1KB.zip":
                            try:
                                # Downloading a file
                                NumberOfDownloadedFiles += 1
                                print("Downloading {} ....".format(file))
                                FtpDownloadedSize += ftp.size(file)
                                ftp.retrbinary("RETR " + file, open("/tmp/" + file, 'wb').write)
                            except:
                                print("Error during downloading a file {}!".format(file))
                                raise
                            # Uploading a file to S3 Bucket
                            try:
                                s3.meta.client.upload_file("/tmp/" + file, bucketname, file)
                                print("File {} uploaded to S3".format(file))
                            except:
                                print("Error uploading file {} !".format(file))
                                raise
                except:
                    print("Error downloading or uploading a file {}!".format(file))
                    raise
            else:
                print("File {} exist in s3 bucket: {}, skipping!".format(file, bucketname))
                pass
        else:
            print("The file is directory {}, skipping".format(file))
            pass

    print("File size downloaded: {} bytes.".format(FtpDownloadedSize))
    pushMetricToCloudwatch("FtpDownloadedSize", "None", FtpDownloadedSize)
    print("Files downloaded: {}.".format(NumberOfDownloadedFiles))
    pushMetricToCloudwatch("NumberOfDownloadedFiles", "Bytes", NumberOfDownloadedFiles)
    return "Run completed succesfully"